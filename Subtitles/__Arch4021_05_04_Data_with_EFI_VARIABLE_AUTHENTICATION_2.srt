1
00:00:00,359 --> 00:00:07,140
so how to access UEFI variables in Linux

2
00:00:04,040 --> 00:00:10,080
Linux kernel exports use UEFI variables

3
00:00:07,140 --> 00:00:13,259
in historically there were two ways it

4
00:00:10,080 --> 00:00:17,160
does it first was systems based method

5
00:00:13,259 --> 00:00:22,260
which was used yearly for EFI and and

6
00:00:17,160 --> 00:00:26,160
UEFI but that method was replaced by EFI

7
00:00:22,260 --> 00:00:30,199
VAR FS fi system which was created to

8
00:00:26,160 --> 00:00:33,540
address some shortcomings of of cisfs

9
00:00:30,199 --> 00:00:36,780
first like one of the main problems with

10
00:00:33,540 --> 00:00:40,160
all suspects mechanis was was the fact

11
00:00:36,780 --> 00:00:44,280
that it supported just one kilobyte

12
00:00:40,160 --> 00:00:48,079
variables and this limitation was was

13
00:00:44,280 --> 00:00:51,300
because it was inherited from the old

14
00:00:48,079 --> 00:00:55,199
0.99 EFI specifications so it was

15
00:00:51,300 --> 00:00:58,620
incredited from pre-ufi time and these

16
00:00:55,199 --> 00:01:02,640
days UEFI variables can be larger even

17
00:00:58,620 --> 00:01:04,979
than single single page in Linux so csfs

18
00:01:02,640 --> 00:01:09,360
is no longer the best interface to

19
00:01:04,979 --> 00:01:12,900
handle those structures and so csfs is

20
00:01:09,360 --> 00:01:15,659
no longer used and will will not be

21
00:01:12,900 --> 00:01:17,700
described in this course these days

22
00:01:15,659 --> 00:01:20,280
operation on variables are performed

23
00:01:17,700 --> 00:01:24,020
through special file system called EFI

24
00:01:20,280 --> 00:01:28,799
bar FS which is typically mounted in

25
00:01:24,020 --> 00:01:32,939
slash slash field model slash EFI slash

26
00:01:28,799 --> 00:01:35,400
EFI RS and UEFI variables in this

27
00:01:32,939 --> 00:01:38,400
directory can be created deleted and

28
00:01:35,400 --> 00:01:41,220
modified deleting and modification might

29
00:01:38,400 --> 00:01:45,240
must be explicitly enabled so by default

30
00:01:41,220 --> 00:01:48,000
it's not possible to accidentally remove

31
00:01:45,240 --> 00:01:50,939
some files from that directory the

32
00:01:48,000 --> 00:01:54,420
reason for that was presence of numerous

33
00:01:50,939 --> 00:01:56,700
firmware box for example box which were

34
00:01:54,420 --> 00:02:00,000
caused by removal or removal of

35
00:01:56,700 --> 00:02:02,759
non-standard UEFI variables some most

36
00:02:00,000 --> 00:02:05,579
likely those variables where a vendor

37
00:02:02,759 --> 00:02:08,280
specific UEFI variables which were using

38
00:02:05,579 --> 00:02:10,619
use it during the boot process because

39
00:02:08,280 --> 00:02:13,860
removing of those variables cost system

40
00:02:10,619 --> 00:02:18,180
hang and because of those issues Linux

41
00:02:13,860 --> 00:02:20,879
developers decided to prevent accidental

42
00:02:18,180 --> 00:02:22,920
removal by setting correct parameters

43
00:02:20,879 --> 00:02:25,440
for files creation and writing to

44
00:02:22,920 --> 00:02:29,959
variables require special treatment for

45
00:02:25,440 --> 00:02:33,540
example string file names in in our

46
00:02:29,959 --> 00:02:36,000
ef5rs directory need a special format

47
00:02:33,540 --> 00:02:40,200
which is first variable name and then

48
00:02:36,000 --> 00:02:43,980
Dash and then vendor GUID and so in in

49
00:02:40,200 --> 00:02:46,140
UEFI Spec we typically provide those as a

50
00:02:43,980 --> 00:02:48,420
separate parameters and we identify

51
00:02:46,140 --> 00:02:52,019
variables through vendor Google

52
00:02:48,420 --> 00:02:54,720
namespace not through not through kind

53
00:02:52,019 --> 00:02:57,360
of connected vendor GUID and variable

54
00:02:54,720 --> 00:03:00,959
name second thing is that file must

55
00:02:57,360 --> 00:03:04,260
start with UEFI variable attributes

56
00:03:00,959 --> 00:03:06,780
and third thing is that writing to the

57
00:03:04,260 --> 00:03:09,540
file must be done with one right

58
00:03:06,780 --> 00:03:12,300
operation so it's kind of carrying about

59
00:03:09,540 --> 00:03:17,099
atomicity of the operation so sudden

60
00:03:12,300 --> 00:03:19,280
power down will not break EFI bars file

61
00:03:17,099 --> 00:03:19,280
system

