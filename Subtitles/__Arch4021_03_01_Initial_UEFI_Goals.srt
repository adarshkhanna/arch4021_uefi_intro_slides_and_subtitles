1
00:00:00,399 --> 00:00:06,879
So just to make sure that we are on

2
00:00:02,639 --> 00:00:08,400
the same page, we're using a UEFI 2.9

3
00:00:06,879 --> 00:00:10,639
version of specification which was

4
00:00:08,400 --> 00:00:13,840
released in March, 2021

5
00:00:10,639 --> 00:00:16,720
and UEFI PI 1.78

6
00:00:13,840 --> 00:00:18,800
which was released in April 2020.

7
00:00:16,720 --> 00:00:20,560
So what was initial

8
00:00:18,800 --> 00:00:22,960
UEFI goal?

9
00:00:20,560 --> 00:00:25,680
So, as we discussed in

10
00:00:22,960 --> 00:00:29,359
previous section we know that EFI was

11
00:00:25,680 --> 00:00:30,800
designed to boot Itanium-based computers.

12
00:00:29,359 --> 00:00:34,480
But,

13
00:00:30,800 --> 00:00:36,160
despite Itanium was first 64-bit CPU

14
00:00:34,480 --> 00:00:38,719
it was totally different from previous

15
00:00:36,160 --> 00:00:41,520
32-bit things and it happened to be

16
00:00:38,719 --> 00:00:43,280
impossible to use old BIOS code. So

17
00:00:41,520 --> 00:00:46,559
legacy BIOS

18
00:00:43,280 --> 00:00:49,280
was stuck with 16-bit real mode

19
00:00:46,559 --> 00:00:51,920
and backward compatibility for

20
00:00:49,280 --> 00:00:53,680
that platform happened to be

21
00:00:51,920 --> 00:00:55,520
a necessary evil.

22
00:00:53,680 --> 00:00:58,079
So, 

23
00:00:55,520 --> 00:01:00,079
EFI was supposed to overcome those

24
00:00:58,079 --> 00:01:02,399
problems but

25
00:01:00,079 --> 00:01:04,640
it happened that it's not so easy to

26
00:01:02,399 --> 00:01:08,320
get rid of all this legacy mode and all

27
00:01:04,640 --> 00:01:09,520
this backward compatibility limitations.

28
00:01:08,320 --> 00:01:11,920


29
00:01:09,520 --> 00:01:14,640
So on the other side we got the

30
00:01:11,920 --> 00:01:15,680
proprietary nature of BIOS

31
00:01:14,640 --> 00:01:18,000
which was

32
00:01:15,680 --> 00:01:20,400
part of the compatibility

33
00:01:18,000 --> 00:01:21,520
problem 

34
00:01:20,400 --> 00:01:22,720
.

35
00:01:21,520 --> 00:01:26,080
But,

36
00:01:22,720 --> 00:01:30,159
the new UEFI also didn't get rid of that

37
00:01:26,080 --> 00:01:33,360
and maybe this was because

38
00:01:30,159 --> 00:01:34,479
the UEFI specification was a brilliant

39
00:01:33,360 --> 00:01:37,280
move to preserve

40
00:01:34,479 --> 00:01:40,240
previously established

41
00:01:37,280 --> 00:01:42,240
supply chain and economy and just to

42
00:01:40,240 --> 00:01:43,680
make sure that the key component will be

43
00:01:42,240 --> 00:01:45,759
in hands of

44
00:01:43,680 --> 00:01:48,560
given parties they established the

45
00:01:45,759 --> 00:01:50,960
UEFI specification. Of course, on the other

46
00:01:48,560 --> 00:01:52,560
side it helped to standardize the

47
00:01:50,960 --> 00:01:55,520
interface between the hardware and the

48
00:01:52,560 --> 00:01:55,520
operating system.



