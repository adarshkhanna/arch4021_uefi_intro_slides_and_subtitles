1
00:00:00,000 --> 00:00:06,060
let's discuss said variable call and how

2
00:00:03,720 --> 00:00:08,400
to create input parameters for it when

3
00:00:06,060 --> 00:00:10,200
using EFI variable authentication 3

4
00:00:08,400 --> 00:00:13,019
descriptor first of all we're starting

5
00:00:10,200 --> 00:00:15,660
with EFI variable authentication 3

6
00:00:13,019 --> 00:00:17,760
descriptor structure creation we have to

7
00:00:15,660 --> 00:00:19,740
choose descriptor type there are two

8
00:00:17,760 --> 00:00:23,640
types as we discussed timestamp and

9
00:00:19,740 --> 00:00:26,460
nouns in case of timestamp it it is very

10
00:00:23,640 --> 00:00:30,180
similar to authentication tool so first

11
00:00:26,460 --> 00:00:34,620
of all we need EFI time structure which

12
00:00:30,180 --> 00:00:37,860
contain a timestamp in case of non-stype

13
00:00:34,620 --> 00:00:41,879
we have to create a secondary descriptor

14
00:00:37,860 --> 00:00:44,520
which will be attached right after the

15
00:00:41,879 --> 00:00:47,100
authentication 3 descriptor this

16
00:00:44,520 --> 00:00:49,739
secondary descriptor is authentication

17
00:00:47,100 --> 00:00:52,680
three nodes descriptor in case of

18
00:00:49,739 --> 00:00:54,840
updating or delete deleting the

19
00:00:52,680 --> 00:00:58,079
authentication three nodes type if we're

20
00:00:54,840 --> 00:01:00,600
doing update or deletion we have to to

21
00:00:58,079 --> 00:01:04,260
provide a nouns buffer of existing

22
00:01:00,600 --> 00:01:07,439
variable for hashing purposes so it is

23
00:01:04,260 --> 00:01:10,760
added to additional content content to

24
00:01:07,439 --> 00:01:14,820
harsh which should already also contain

25
00:01:10,760 --> 00:01:17,340
new variable value if we're not doing

26
00:01:14,820 --> 00:01:21,000
update or deletion we're just skipping

27
00:01:17,340 --> 00:01:23,759
that step then we checking the variable

28
00:01:21,000 --> 00:01:27,780
authentication flux the authentication

29
00:01:23,759 --> 00:01:31,560
tree flux if we got the enhanced out

30
00:01:27,780 --> 00:01:35,100
flag update search flag set then we have

31
00:01:31,560 --> 00:01:37,320
to provide new certificate to our

32
00:01:35,100 --> 00:01:40,020
additional content to Hash we have to

33
00:01:37,320 --> 00:01:42,900
concatenate it in our additional content

34
00:01:40,020 --> 00:01:45,659
to Hash in case if flag is not set then

35
00:01:42,900 --> 00:01:49,200
we just moving forward to hashing in

36
00:01:45,659 --> 00:01:51,960
case of producing cash for for their

37
00:01:49,200 --> 00:01:54,299
signing we provide us in case of

38
00:01:51,960 --> 00:01:57,540
authenticated to a variable name

39
00:01:54,299 --> 00:02:00,540
vendorgoid attributes our secondary

40
00:01:57,540 --> 00:02:02,820
descriptor if depend which depends on

41
00:02:00,540 --> 00:02:06,240
the type either we have timestamp either

42
00:02:02,820 --> 00:02:09,000
we have nones new variable content and

43
00:02:06,240 --> 00:02:12,720
whatever additional content to Hash we

44
00:02:09,000 --> 00:02:14,780
have based on the flux or based on the

45
00:02:12,720 --> 00:02:18,540
type of the

46
00:02:14,780 --> 00:02:21,060
descriptor then we sign this hash we

47
00:02:18,540 --> 00:02:24,599
create create win certificate UEFI quit

48
00:02:21,060 --> 00:02:27,900
with certificate type pkcs7 then we

49
00:02:24,599 --> 00:02:30,780
construct that encoded sidenet data

50
00:02:27,900 --> 00:02:34,560
structure which contain uh previously

51
00:02:30,780 --> 00:02:37,080
Signet hash and then we update the EFI

52
00:02:34,560 --> 00:02:40,920
variable authentication metadata size

53
00:02:37,080 --> 00:02:44,400
with information about metadata sites so

54
00:02:40,920 --> 00:02:47,220
this is whole size except size of our

55
00:02:44,400 --> 00:02:51,780
variable new content then we concaten a

56
00:02:47,220 --> 00:02:55,379
serialized descriptor with the variable

57
00:02:51,780 --> 00:02:58,220
new content and we provide that to a set

58
00:02:55,379 --> 00:02:58,220
variable call

