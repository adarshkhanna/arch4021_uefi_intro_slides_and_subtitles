1
00:00:00,000 --> 00:00:06,480
let's look closer at our ovm ffd file

2
00:00:03,600 --> 00:00:08,639
and try to identify components that we

3
00:00:06,480 --> 00:00:11,000
discussed in ufi design principles

4
00:00:08,639 --> 00:00:11,000
section

5
00:00:15,960 --> 00:00:22,820
so first let's load again our ovmf FD

6
00:00:27,240 --> 00:00:34,559
so top component is identified as a four

7
00:00:31,019 --> 00:00:38,940
megabyte ufi image as we said before y4

8
00:00:34,559 --> 00:00:43,020
Max we see here the full size is 400 000

9
00:00:38,940 --> 00:00:46,920
hex and typically on the top level we

10
00:00:43,020 --> 00:00:50,280
see firbar device or whole file which

11
00:00:46,920 --> 00:00:53,039
represent what would be flushed into our

12
00:00:50,280 --> 00:00:57,000
firmware storage so SPI flash for

13
00:00:53,039 --> 00:01:00,840
example so that's why our file is has

14
00:00:57,000 --> 00:01:03,359
extension FD just indicate to tell us

15
00:01:00,840 --> 00:01:05,939
that this is full fieldwork device and

16
00:01:03,359 --> 00:01:10,080
fieldwork devices described in ufi

17
00:01:05,939 --> 00:01:13,260
specification at other design component

18
00:01:10,080 --> 00:01:16,619
inside a firmware device or inside this

19
00:01:13,260 --> 00:01:19,260
image that we loaded we can find

20
00:01:16,619 --> 00:01:22,500
other components which are called

21
00:01:19,260 --> 00:01:25,439
volumes so we can see we have three

22
00:01:22,500 --> 00:01:29,520
volumes here and our top volume is uh

23
00:01:25,439 --> 00:01:33,299
and vram and vram is uh is a special

24
00:01:29,520 --> 00:01:35,520
volume uh related to storing qf5 enables

25
00:01:33,299 --> 00:01:39,299
which we will discuss later in this

26
00:01:35,520 --> 00:01:41,880
course other two volumes are containing

27
00:01:39,299 --> 00:01:43,920
a firmware file system version 2 and

28
00:01:41,880 --> 00:01:48,360
that means that they just contain more

29
00:01:43,920 --> 00:01:51,060
files that can be understood by by ufi

30
00:01:48,360 --> 00:01:54,060
firmware and those of course are

31
00:01:51,060 --> 00:01:56,820
explained in ufi specification in way

32
00:01:54,060 --> 00:01:59,960
way more details including all the

33
00:01:56,820 --> 00:02:03,240
information that we see in the headers

34
00:01:59,960 --> 00:02:06,180
recognizes in the headers by the ufi

35
00:02:03,240 --> 00:02:09,660
tool so what is important to know that

36
00:02:06,180 --> 00:02:13,379
field modifier systems can be nested so

37
00:02:09,660 --> 00:02:16,920
we can see that in this volume if we

38
00:02:13,379 --> 00:02:20,580
unroll it we can see

39
00:02:16,920 --> 00:02:22,739
there are there's another firmware file

40
00:02:20,580 --> 00:02:25,319
system here and another refrigerator

41
00:02:22,739 --> 00:02:28,860
file system here and those are under

42
00:02:25,319 --> 00:02:31,800
this top firmware file system so inside

43
00:02:28,860 --> 00:02:34,379
firmware file system uh we can have

44
00:02:31,800 --> 00:02:37,560
another volumes which contain a field

45
00:02:34,379 --> 00:02:40,800
modifier system and how to figure out uh

46
00:02:37,560 --> 00:02:44,400
what's inside this file system we can

47
00:02:40,800 --> 00:02:47,660
take guides which is volume good here

48
00:02:44,400 --> 00:02:52,920
and we can try to look in our

49
00:02:47,660 --> 00:02:56,760
FDF file which describes content of the

50
00:02:52,920 --> 00:02:57,840
FD file produce it during the build

51
00:02:56,760 --> 00:03:00,200
process

52
00:02:57,840 --> 00:03:03,500
so let's go to the

53
00:03:00,200 --> 00:03:06,660
ovmf PKG

54
00:03:03,500 --> 00:03:08,040
x64 FDF because this is what we used for

55
00:03:06,660 --> 00:03:10,800
building

56
00:03:08,040 --> 00:03:12,659
and let's search for this guid and you

57
00:03:10,800 --> 00:03:16,680
we can see here

58
00:03:12,659 --> 00:03:19,500
um that there is a

59
00:03:16,680 --> 00:03:22,560
firmware volume Pei firmware volume so

60
00:03:19,500 --> 00:03:26,959
this firmware volume most likely contain

61
00:03:22,560 --> 00:03:30,360
a pi related files and we can see

62
00:03:26,959 --> 00:03:33,180
below what kind of a primary file was

63
00:03:30,360 --> 00:03:35,760
used and what kind of modules we can

64
00:03:33,180 --> 00:03:38,519
find here so we probably should find

65
00:03:35,760 --> 00:03:41,900
some PCD related module we should find

66
00:03:38,519 --> 00:03:45,599
some report status code

67
00:03:41,900 --> 00:03:47,900
related module and or pay core

68
00:03:45,599 --> 00:03:52,739
so if we look inside

69
00:03:47,900 --> 00:03:56,459
this we see PCD this is what we expected

70
00:03:52,739 --> 00:03:59,340
report status code router pi and so this

71
00:03:56,459 --> 00:04:00,480
is what we expected based on the FDF

72
00:03:59,340 --> 00:04:03,720
file

73
00:04:00,480 --> 00:04:07,680
um as as you can see inside inside this

74
00:04:03,720 --> 00:04:11,299
uh pay firmware volume we see pay

75
00:04:07,680 --> 00:04:15,840
modules which are also a component

76
00:04:11,299 --> 00:04:18,979
described in ufi specification so that's

77
00:04:15,840 --> 00:04:22,199
that's one type of component

78
00:04:18,979 --> 00:04:24,240
we can also look into the second

79
00:04:22,199 --> 00:04:29,759
firmware volume

80
00:04:24,240 --> 00:04:32,460
this one if we will look at it in FDF

81
00:04:29,759 --> 00:04:35,940
we will figure out that this is

82
00:04:32,460 --> 00:04:39,120
this is Dixie firmware volume so what

83
00:04:35,940 --> 00:04:41,699
would be expected here are Dixie drivers

84
00:04:39,120 --> 00:04:44,880
and we can see there is Dixie car and

85
00:04:41,699 --> 00:04:46,699
there's runtime dxc and various other uh

86
00:04:44,880 --> 00:04:50,460
Dixie drivers

87
00:04:46,699 --> 00:04:52,919
you can ask where's the stack so what is

88
00:04:50,460 --> 00:04:56,460
the SEC phase

89
00:04:52,919 --> 00:04:58,020
a sacrifice is here and it's just just

90
00:04:56,460 --> 00:05:02,340
one

91
00:04:58,020 --> 00:05:05,699
um just one file it's recognized by ufi

92
00:05:02,340 --> 00:05:07,259
Tool but it's just one file also what is

93
00:05:05,699 --> 00:05:09,900
important getting back a little bit to

94
00:05:07,259 --> 00:05:12,240
Dixie that there are not only Dixie

95
00:05:09,900 --> 00:05:15,660
drivers here there are also applications

96
00:05:12,240 --> 00:05:19,560
and this application UI UI app is

97
00:05:15,660 --> 00:05:23,340
special application because this is our

98
00:05:19,560 --> 00:05:27,720
bios setup money which will pop up when

99
00:05:23,340 --> 00:05:31,500
we hit exit from the ufi shell or or hit

100
00:05:27,720 --> 00:05:34,800
some special hotkey during the boot

101
00:05:31,500 --> 00:05:38,220
process and that would be it about

102
00:05:34,800 --> 00:05:41,660
design components that we can find in

103
00:05:38,220 --> 00:05:41,660
our ovmf FD

