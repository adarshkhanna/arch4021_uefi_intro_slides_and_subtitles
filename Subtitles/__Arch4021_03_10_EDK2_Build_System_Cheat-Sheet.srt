1
00:00:00,080 --> 00:00:07,600
Let's discuss EDK2 build system.

2
00:00:04,000 --> 00:00:10,080
So, EDK2 has its own build system

3
00:00:07,600 --> 00:00:12,480
most of that resides in

4
00:00:10,080 --> 00:00:14,240
BaseTools. There are also some

5
00:00:12,480 --> 00:00:17,840
configuration files which can be

6
00:00:14,240 --> 00:00:18,640
generated and used from Conf directory

7
00:00:17,840 --> 00:00:20,880
but

8
00:00:18,640 --> 00:00:24,000
BaseTools contain most of the Python

9
00:00:20,880 --> 00:00:25,680
scripts required to to build and

10
00:00:24,000 --> 00:00:29,359
to consume

11
00:00:25,680 --> 00:00:31,840
special EDK2 files, .dec, .dsc

12
00:00:29,359 --> 00:00:34,320
.inf, .fdf files and those files are just

13
00:00:31,840 --> 00:00:37,360
consumed and converted into the make

14
00:00:34,320 --> 00:00:40,960
files, and based on the tool chain

15
00:00:37,360 --> 00:00:42,559
defined in conf directory in special

16
00:00:40,960 --> 00:00:44,640
tools dev file

17
00:00:42,559 --> 00:00:47,680
the

18
00:00:44,640 --> 00:00:50,640
source code is compiled and then we're

19
00:00:47,680 --> 00:00:55,039
getting our firmware image.

20
00:00:50,640 --> 00:00:58,399
So, every module which has

21
00:00:55,039 --> 00:01:00,879
.inf file inside,

22
00:00:58,399 --> 00:01:03,199
contains some metadata so this .inf file

23
00:01:00,879 --> 00:01:05,040
contains some meta data. There

24
00:01:03,199 --> 00:01:06,960
is name of the module there is version

25
00:01:05,040 --> 00:01:09,040
of the module there is list of source

26
00:01:06,960 --> 00:01:10,240
files that have to be used to compile

27
00:01:09,040 --> 00:01:12,880
this module

28
00:01:10,240 --> 00:01:15,200
there is function which is called

29
00:01:12,880 --> 00:01:16,960
entry point which is kind of entry to

30
00:01:15,200 --> 00:01:20,159
given

31
00:01:16,960 --> 00:01:21,520
part of the code to given file.

32
00:01:20,159 --> 00:01:23,360


33
00:01:21,520 --> 00:01:26,400
Maybe this is this is more like an

34
00:01:23,360 --> 00:01:28,560
internal part but every module

35
00:01:26,400 --> 00:01:30,479
got the entry function

36
00:01:28,560 --> 00:01:32,159
which is called entry point.

37
00:01:30,479 --> 00:01:33,600
There are some dependencies described

38
00:01:32,159 --> 00:01:35,840
there,

39
00:01:33,600 --> 00:01:37,759
what packages have to be included

40
00:01:35,840 --> 00:01:39,520
because some

41
00:01:37,759 --> 00:01:41,360
libraries or protocols or other

42
00:01:39,520 --> 00:01:43,920
components of

43
00:01:41,360 --> 00:01:45,759
of EDK2 may be used inside.

44
00:01:43,920 --> 00:01:47,200
There are some modules defined that some

45
00:01:45,759 --> 00:01:49,360
other modules defined there are some

46
00:01:47,200 --> 00:01:52,840
GUIDs.

47
00:01:49,360 --> 00:01:54,479
So the .inf file is quite

48
00:01:52,840 --> 00:01:56,399


49
00:01:54,479 --> 00:01:58,560
interesting piece of

50
00:01:56,399 --> 00:02:00,000
definition for the module.

51
00:01:58,560 --> 00:02:02,880
This .inf file

52
00:02:00,000 --> 00:02:05,040
usage and how it looks like is very

53
00:02:02,880 --> 00:02:07,600
similar to the model used by Microsoft

54
00:02:05,040 --> 00:02:10,319
Windows, so anyone who did driver

55
00:02:07,600 --> 00:02:12,080
development for my Microsoft Windows

56
00:02:10,319 --> 00:02:13,520
should already know those files very

57
00:02:12,080 --> 00:02:16,160
well.

58
00:02:13,520 --> 00:02:19,360
Output of those files after compilation

59
00:02:16,160 --> 00:02:23,040
after building is portable executable

60
00:02:19,360 --> 00:02:24,000
common object file format. So PE COFF

61
00:02:23,040 --> 00:02:27,200
also

62
00:02:24,000 --> 00:02:29,360
should be well known for the

63
00:02:27,200 --> 00:02:32,800
Microsoft Windows

64
00:02:29,360 --> 00:02:34,800
environment programmers.

65
00:02:32,800 --> 00:02:37,040
Those modules as I said are organized in

66
00:02:34,800 --> 00:02:40,400
packages, packages are described using

67
00:02:37,040 --> 00:02:43,680
metadata storage in the .dsc and .dec files.

68
00:02:40,400 --> 00:02:47,120
And finally in BaseTools we got also

69
00:02:43,680 --> 00:02:49,680
edksetup.sh bash script which is used for

70
00:02:47,120 --> 00:02:51,920
setting up environment for our

71
00:02:49,680 --> 00:02:55,360
building system. So

72
00:02:51,920 --> 00:02:58,480
in Unix operating system

73
00:02:55,360 --> 00:03:01,440
we just call edksetup.sh, and and it

74
00:02:58,480 --> 00:03:04,480
set up some environmental variable use

75
00:03:01,440 --> 00:03:08,959
during the compilation process. One of

76
00:03:04,480 --> 00:03:11,200
this variable is EDK_TOOLS_PATH and

77
00:03:08,959 --> 00:03:12,480
this one points to the BaseTools

78
00:03:11,200 --> 00:03:15,280
directory,

79
00:03:12,480 --> 00:03:16,640
and this base tool directory contains

80
00:03:15,280 --> 00:03:20,239
various other

81
00:03:16,640 --> 00:03:23,120
components needed for building EDK2.

82
00:03:20,239 --> 00:03:26,159
The typical build command

83
00:03:23,120 --> 00:03:30,159
template can look as follows

84
00:03:26,159 --> 00:03:33,760
so build -a, where we provide arch

85
00:03:30,159 --> 00:03:36,959
which is our 32-bit or 64-bit

86
00:03:33,760 --> 00:03:37,840
-p and then we provide the .dsc file

87
00:03:36,959 --> 00:03:41,040
path

88
00:03:37,840 --> 00:03:42,239
this .dsc of course describe our

89
00:03:41,040 --> 00:03:44,480
package

90
00:03:42,239 --> 00:03:46,799
which can be

91
00:03:44,480 --> 00:03:49,840
either some sub component or their

92
00:03:46,799 --> 00:03:52,239
whole build for given platform. There

93
00:03:49,840 --> 00:03:53,280
can be also some flags some defines after

94
00:03:52,239 --> 00:03:54,480
that

95
00:03:53,280 --> 00:03:55,680
-t

96
00:03:54,480 --> 00:03:58,080


97
00:03:55,680 --> 00:04:02,000
and then we provide tool chain which can

98
00:03:58,080 --> 00:04:04,720
be gcc or some other compiler

99
00:04:02,000 --> 00:04:08,280
and -b build type which can be

100
00:04:04,720 --> 00:04:08,280
release or debug.




