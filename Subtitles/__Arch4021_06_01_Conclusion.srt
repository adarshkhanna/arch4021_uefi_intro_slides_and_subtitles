1
00:00:00,000 --> 00:00:06,420
congratulations you get to the end of

2
00:00:02,280 --> 00:00:08,160
Arc 4021 introductory UEFI let's quickly

3
00:00:06,420 --> 00:00:10,920
recap what we learned during this course

4
00:00:08,160 --> 00:00:12,960
we started with firmware fundamentals we

5
00:00:10,920 --> 00:00:16,560
learned what film means why we need it

6
00:00:12,960 --> 00:00:19,619
how it is related to BIOS term what BIOS

7
00:00:16,560 --> 00:00:23,580
term means and how BIOS evolved into the

8
00:00:19,619 --> 00:00:25,880
UEFI we then started learning a little

9
00:00:23,580 --> 00:00:28,680
bit about UEFI Basics

10
00:00:25,880 --> 00:00:30,900
especially the goals of UEFI NPI

11
00:00:28,680 --> 00:00:33,540
specifications what kind of features

12
00:00:30,900 --> 00:00:37,559
they describe what kind of essential

13
00:00:33,540 --> 00:00:40,500
components we can find inside we we

14
00:00:37,559 --> 00:00:43,079
learned about UEFI architecture and we

15
00:00:40,500 --> 00:00:45,300
also learned about UEFI implementations

16
00:00:43,079 --> 00:00:48,120
especially about reference

17
00:00:45,300 --> 00:00:50,879
implementation called EDK2

18
00:00:48,120 --> 00:00:54,420
from the Practical point of view we we

19
00:00:50,879 --> 00:00:57,539
know how to build and debug indicator we

20
00:00:54,420 --> 00:01:00,420
know how to explore uh UEFI BIOS images

21
00:00:57,539 --> 00:01:05,540
we know how to boot those BIOS images

22
00:01:00,420 --> 00:01:07,460
into emulated environment using QA qimo

23
00:01:05,540 --> 00:01:10,799
we know

24
00:01:07,460 --> 00:01:13,200
UEFI boot process and know how to explore

25
00:01:10,799 --> 00:01:16,680
it know where to find the logs about it

26
00:01:13,200 --> 00:01:19,860
and how to read it we also know UEFI

27
00:01:16,680 --> 00:01:22,320
shell some basic commands how to deal

28
00:01:19,860 --> 00:01:25,640
with them or how to find help about them

29
00:01:22,320 --> 00:01:30,000
and what are what are the most important

30
00:01:25,640 --> 00:01:33,299
UEFI shell commands we also know how to

31
00:01:30,000 --> 00:01:36,180
store UEFI BIOS configuration and we know

32
00:01:33,299 --> 00:01:38,880
various types of UEFI variables which can

33
00:01:36,180 --> 00:01:41,220
keep that configuration we know how to

34
00:01:38,880 --> 00:01:45,240
deal with UEFI variables from Linux

35
00:01:41,220 --> 00:01:49,259
environment and and finally we

36
00:01:45,240 --> 00:01:52,560
understand how UEFI variables can relate

37
00:01:49,259 --> 00:01:54,659
to more complex things like UEFI secure

38
00:01:52,560 --> 00:01:55,759
boot so thank you very much for taking

39
00:01:54,659 --> 00:01:59,820
this class

40
00:01:55,759 --> 00:02:01,799
please feel free to explore other open

41
00:01:59,820 --> 00:02:06,000
security training classes

42
00:02:01,799 --> 00:02:08,880
and and we wish you good luck on your

43
00:02:06,000 --> 00:02:11,520
path to further learning and further

44
00:02:08,880 --> 00:02:15,620
exploration of firmware firmware

45
00:02:11,520 --> 00:02:15,620
security and other low-level bits

