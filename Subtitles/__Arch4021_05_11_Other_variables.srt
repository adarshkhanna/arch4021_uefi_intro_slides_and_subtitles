1
00:00:00,000 --> 00:00:06,600
finally I would like to discuss a couple

2
00:00:02,899 --> 00:00:08,519
other variables especially to point how

3
00:00:06,600 --> 00:00:10,920
those can be different so for example

4
00:00:08,519 --> 00:00:13,500
very interesting variable is capsule

5
00:00:10,920 --> 00:00:16,619
Capital update variable those are

6
00:00:13,500 --> 00:00:19,500
capsule with the with the number or

7
00:00:16,619 --> 00:00:21,600
capsule Max variables those variables

8
00:00:19,500 --> 00:00:23,640
contain reports related to capsule

9
00:00:21,600 --> 00:00:27,660
update and the highness which is used

10
00:00:23,640 --> 00:00:29,939
for UEFI firmware update as and as as we

11
00:00:27,660 --> 00:00:32,220
can see the structures are completely

12
00:00:29,939 --> 00:00:34,739
different I don't want to go in into

13
00:00:32,220 --> 00:00:36,239
details with that that's not the point

14
00:00:34,739 --> 00:00:39,420
here if you want to read more about

15
00:00:36,239 --> 00:00:42,360
capsule update there are headers in EDK2

16
00:00:39,420 --> 00:00:45,660
source code which can explain way more

17
00:00:42,360 --> 00:00:47,640
other potential UEFI variable can be

18
00:00:45,660 --> 00:00:49,739
platform specific variable which we

19
00:00:47,640 --> 00:00:52,820
found in in values implementation like

20
00:00:49,739 --> 00:00:55,620
platform CPU info which this variable

21
00:00:52,820 --> 00:00:58,980
contain quite a lot of information which

22
00:00:55,620 --> 00:01:02,100
are returned by cpuid command this can

23
00:00:58,980 --> 00:01:06,479
be useful just to avoid calling again

24
00:01:02,100 --> 00:01:10,979
and again a CPU ID assembly and as we

25
00:01:06,479 --> 00:01:13,979
can see it was integrated in in EDK2 in

26
00:01:10,979 --> 00:01:16,320
alt 3 for for some specific Hardware

27
00:01:13,979 --> 00:01:18,479
Target value view

28
00:01:16,320 --> 00:01:21,360
um Intel Hardware platform the point

29
00:01:18,479 --> 00:01:23,640
here is to avoid calling CPU I do again

30
00:01:21,360 --> 00:01:26,280
and again and to reuse that variable

31
00:01:23,640 --> 00:01:29,220
during the boot process for making this

32
00:01:26,280 --> 00:01:31,979
various decision about loading drivers

33
00:01:29,220 --> 00:01:33,840
and and behavior that should be and

34
00:01:31,979 --> 00:01:35,820
passing parameters to those drivers and

35
00:01:33,840 --> 00:01:37,560
behavior that should be enforced during

36
00:01:35,820 --> 00:01:41,579
the boot process

37
00:01:37,560 --> 00:01:44,220
finally we can talk about Con in con out

38
00:01:41,579 --> 00:01:47,759
Con Air console related variables which

39
00:01:44,220 --> 00:01:51,119
we already mentioned those are mostly

40
00:01:47,759 --> 00:01:54,439
created out of EFI device part protocol

41
00:01:51,119 --> 00:01:58,640
structures we just point to the device

42
00:01:54,439 --> 00:02:02,340
for example serial output or display

43
00:01:58,640 --> 00:02:05,460
where the characters or where the output

44
00:02:02,340 --> 00:02:09,020
should be should be sent there are also

45
00:02:05,460 --> 00:02:12,300
a con in Dev con out Dev and con aerodaf

46
00:02:09,020 --> 00:02:17,160
volatile volatile volatile variables

47
00:02:12,300 --> 00:02:20,280
which just describe actual choice of the

48
00:02:17,160 --> 00:02:23,640
device which is used for input for our

49
00:02:20,280 --> 00:02:26,099
output or for error Printing and those

50
00:02:23,640 --> 00:02:28,160
variables which we described in in this

51
00:02:26,099 --> 00:02:31,020
and previous sections are just examples

52
00:02:28,160 --> 00:02:34,800
many variables are platform

53
00:02:31,020 --> 00:02:38,220
implementation specific and it is always

54
00:02:34,800 --> 00:02:40,080
good practice for firmware developers

55
00:02:38,220 --> 00:02:43,800
Firma researchers security researchers

56
00:02:40,080 --> 00:02:46,500
to just simply check your using your

57
00:02:43,800 --> 00:02:48,900
preferred method what kind of variables

58
00:02:46,500 --> 00:02:53,780
are available in your platform and if

59
00:02:48,900 --> 00:02:53,780
those can be useful for your purposes

